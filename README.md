# MOST Chi.Bio paper

The tex files for the chi.bio paper 
<object data="https://gitlab.com/ebmichel/most-chi.bio-paper/-/raw/main/HardwareXTemplate_20210707.pdf?inline=false" type="application/pdf" width="700px" height="700px">
    <embed src="https://gitlab.com/ebmichel/most-chi.bio-paper/-/raw/main/HardwareXTemplate_20210707.pdf?inline=false">
        <p>Download the PDF to view current version: <a href="https://gitlab.com/ebmichel/most-chi.bio-paper/-/raw/main/HardwareXTemplate_20210707.pdf?inline=false">Download PDF</a>.</p>
    </embed>
</object>
